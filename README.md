#**README**


##**Captura de l'aplicació**
![](./captura1.png)

##**Link al manual**
(https://www.digitalocean.com/community/tutorials/how-to-deploy-a-symfony-application-to-production-on-ubuntu-14-04)

##**Explicació dels errors**
L'únic problema que m'ha donat el manual ha sigut el canviar la versió de php de 5 a 7.0, havent de canviar algunes ordres, i que al pas 2 en comptes del fitxer "sudo nano /etc/mysql/my.cnf" s'ha de canviar el fitxer "/etc/mysql/mysql.conf.d/mysqld.cnf"
